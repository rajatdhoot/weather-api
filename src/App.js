import React, { useState, useEffect } from "react";
import Home from "./Home";
import "bootstrap/dist/css/bootstrap.min.css";
import WeatherCard from "./Weather";
import { WeatherApi } from "./api";
import { useGeolocation } from "react-use";
import { Spinner } from "react-bootstrap";
import Graph from "./Graph";
function App() {
  const { longitude, latitude, loading, error } = useGeolocation();
  console.warn(useGeolocation());
  const [searchText, setSearchText] = useState("");
  const [weatherData, setWeatherData] = useState({});
  const [page, setPage] = useState("home");
  const [state, setState] = useState({
    loading: false,
    latitude: "",
    longitude: ""
  });

  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    setSearchText(value);
  };

  useEffect(() => {
    if (latitude || longitude) {
      if (latitude !== state.latitude || longitude !== state.longitude) {
        setState({
          loading: true,
          latitude,
          longitude
        });
        WeatherApi({ latitude, longitude })
          .then(data => {
            setState(prevState => ({
              ...prevState,
              loading: false
            }));
            setWeatherData(data);
          })
          .catch(err => {
            setState(prevState => ({
              ...prevState,
              loading: false
            }));
          });
      }
    }
  }, [longitude, latitude, state]);

  return (
    <div>
      <div>{loading && "Getting User Location"}</div>
      <div>
        {error && error.message}
      </div>
      {page !== "Graph" && (
        <Home searchText={searchText} handleChange={handleChange} />
      )}

      {page !== "Graph" && searchText === "weather" && (
        <>
          {state.loading ? (
            <div className="d-flex justify-content-center">
              <Spinner animation="border" />
            </div>
          ) : (
            <WeatherCard setPage={setPage} weather={weatherData} />
          )}
        </>
      )}
      {page === "Graph" && (
        <Graph setPage={setPage} data={weatherData.hourly.data} />
      )}
    </div>
  );
}
export default App;
