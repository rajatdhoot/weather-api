import React, { useEffect } from "react";
import { usePosition } from "use-position";

const Location = ({ setLocation }) => {
  const { latitude, longitude, timestamp, accuracy, error } = usePosition(
    true,
    { enableHighAccuracy: true }
  );

  useEffect(() => {
    setLocation({ latitude, longitude, timestamp, accuracy, error });
  }, [latitude, longitude]);
  return null;
};

export default Location;
