import React from "react";
import { ProgressBar, Card, Button } from "react-bootstrap";
import styled from "styled-components";
const Graph = ({ data, setPage }) => {
  const handleBack = () => {
    setPage("home");
  };
  return (
    <div>
      <Card className="text-center">
        <Card.Body>
          <Card.Title>Temperature Graph</Card.Title>
          <ProgressWrapper>
            {data.map(({ temperature, time }) => (
              <ProgressBar
              key={time}
                striped
                className="my-2 ml-3"
                label={`${temperature}%`}
                now={temperature}
              />
            ))}
          </ProgressWrapper>
        </Card.Body>
        <Button onClick={handleBack}>Back</Button>
      </Card>
    </div>
  );
};

const ProgressWrapper = styled.div`
  border-left: 5px solid grey;
`;
export default Graph;
