import React from "react";
import { Card, CardColumns } from "react-bootstrap";
import styled from "styled-components";
import clearday from "./Images/clearday.jpg";
import partlyCloudyDay from "./Images/partlyCloudyDay.jpeg";

const GetImage = image => {
  switch (image) {
    case "partly-cloudy-day":
      return partlyCloudyDay;
    case "clear-day":
      return clearday;
    default:
      return null;
  }
};

const WeatherCard = ({ weather, setPage }) => {
  const handleOnClick = () => {
    setPage("Graph");
  };
  return (
    <CardWrapper onClick={handleOnClick}>
      <Card.Img alt={weather.icon} variant="top" src={GetImage(weather.icon)} />
      <Card.Body>
        <Card.Title>{weather.summary}</Card.Title>
        <FlexDiv>
          <div>Temperature High</div>
          <div>{weather.temperatureHigh}</div>
        </FlexDiv>
        <FlexDiv>
          <div>Temperature Low</div>
          <div>{weather.temperatureLow}</div>
        </FlexDiv>
      </Card.Body>
    </CardWrapper>
  );
};

const Weather = ({ setPage, weather: { daily = {} } }) => {
  return (
    <div>
      <CardColumns>
        {daily.data &&
          daily.data.map(weather => (
            <WeatherCard
              key={weather.time}
              setPage={setPage}
              weather={weather}
            />
          ))}
      </CardColumns>
    </div>
  );
};

const FlexDiv = styled.div`
  display: flex;
  align-items: ${props => props.alignItems || "normal"};
`;

const CardWrapper = styled(Card)`
  cursor: pointer;
`;

export default Weather;
