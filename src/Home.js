import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import styled from "styled-components";

const Home = ({ handleChange, searchText }) => {
  return (
    <HomeWrapper
      weather={searchText === "weather"}
      className="container-fluid"
    >
      <Form.Control
        onChange={handleChange}
        name="search"
        type="text"
        value={searchText}
        placeholder="Search Here"
      />
    </HomeWrapper>
  );
};

const HomeWrapper = styled.div`
  height: ${props => (props.weather ? "50vh" : "100vh")};
  display: flex;
  justify-content: center;
  align-items: center;
`;

export default Home;
